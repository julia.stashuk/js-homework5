// 1. Опишіть своїми словами, що таке метод об'єкту
// Метод об'єкту - це функція, яка є властивістю об’єкту і виконується в контексті даного об'єкта.
// Вона може змінювати його властивості або повертати відповідний результат.
//
// 2. Який тип даних може мати значення властивості об'єкта?
// Будь-який тип: рядок, число, null, undefined, булеве значення, інший об`єкт тощо.
//
// 3. Об'єкт це посилальний тип даних. Що означає це поняття?
// Об'єкт є посилальним типом даних. Це означає, що змінні, які містять об`єкти, насправді посилаються на об'єкт у пам'яті ПК, 
// а не містять його копію. Це означає, що змінна, яка має посилальний тип даних, фактично не містить значення. 
// Тобто робота з даними об'єкта ведеться виключно через посилання. 



function createNewUser(firstName, lastName) {
firstName = prompt("What is your first name?").trim();
lastName = prompt("What is your last name?").trim();

return {
    firstName: firstName,
    lastName: lastName,
    getLogin: function () {
    let fullName = this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    return fullName
    }
}
}

let newUser = createNewUser();
  
console.log(newUser);
console.log(newUser.getLogin());
  